package com.lpl.daily_tfe;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {
	private EditText ed1;
	private EditText add1;
	private EditText add2;
	private Button bt1;
	private Button bt2;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		ed1 = (EditText) findViewById(R.id.add_ething);
		add1 = (EditText) findViewById(R.id.add_sDe);
		add2 = (EditText) findViewById(R.id.add_sTe);
		bt1 = (Button) findViewById(R.id.add_sDb);
		bt2 = (Button) findViewById(R.id.add_sTb);
		bt1.setOnClickListener(this);
		bt2.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_ovr:
				Log.i("tag", "asdfasdfasdffasf");
				Intent in = new Intent();
				//passClass pc=new passClass(add1.getText().toString(),add2.getText().toString(),ed1.getText().toString());
				in.putExtra("Date", add1.getText());
				in.putExtra("Time", add2.getText());
				in.putExtra("Thing", ed1.getText());
				AddActivity.this.setResult(RESULT_OK, in);
				AddActivity.this.finish();
				break;
			case android.R.id.home:
				setResult(0);
				finish();
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View view) {
		Log.i("tass", view.getId() + "");
		Log.i("tass", R.id.add_sTb + "");
		switch (view.getId()) {
			case R.id.add_sDb:
				Calendar cal = Calendar.getInstance();
				new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
					@Override
					public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
						add1.setText(i + "-" + i1 + "-" + i2);
					}
				}, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
				break;
			case R.id.add_sTb:
				Calendar cal1 = Calendar.getInstance();
				new TimePickerDialog(AddActivity.this, new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker timePicker, int i, int i1) {
						add2.setText(i + ":" + i1);
					}
				}, cal1.get(Calendar.HOUR_OF_DAY), cal1.get(Calendar.MINUTE), true).show();
				break;
		}
	}
}
