package com.lpl.daily_tfe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener {
	private ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lv = (ListView) findViewById(R.id.list_main);
		SimpleAdapter sm = new SimpleAdapter(this, getData(),
				R.layout.main_list,
				new String[]{"list_img", "list_thing", "list_time"},
				new int[]{R.id.list_img, R.id.list_thing, R.id.list_time});
		lv.setAdapter(sm);
	}

	private List<Map<String, Object>> getData() {
		List<Map<String, Object>> GData = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < 20; i++) {
			Map<String, Object> ms = new HashMap<String, Object>();
			ms.put("list_img", R.mipmap.ic_launcher);
			ms.put("list_thing", "我是标题" + i);
			ms.put("list_time", "我是日期" + i);
			GData.add(ms);
		}
		return GData;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
			case 999:
				break;
			case RESULT_OK:
				String D = data.getExtras().getString("Date");
				String T = data.getExtras().getString("Time");
				String H = data.getExtras().getString("String");

				break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreatePanelMenu(int featureId, Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return super.onCreatePanelMenu(featureId, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_add:
				Intent in = new Intent(MainActivity.this, AddActivity.class);
				startActivityForResult(in, 1);
				break;
			case R.id.menu_out:
				finish();
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
		Log.i("xyz", view.getId() + "");
		return true;
	}
}
